<?php

namespace Kloo\Infrastructure\Exceptions;

class QueueFailedException extends SystemException
{
  public $event_type = "queue";
}
