<?php

namespace Kloo\Infrastructure\Constants;

class LaravelAuditEventsConstant
{
    public const CREATED ="created";

    public const UPDATED ="updated";

    public const DELETED ="deleted";

    public const RESTORED ="restored";
}