<?php

namespace Kloo\Infrastructure\Constants;

class AppConstants
{
    const NO_OF_RECORDS_PER_PAGE = 10;
}