<?php

namespace Kloo\Infrastructure\Middlewares;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Kloo\Infrastructure\Log\Logger;
use Kloo\Infrastructure\Response\ResponseTransformer;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Illuminate\Http\Response;

class ConvertToJson
{
    public function handle(Request $request, \Closure $next): JsonResponse|BinaryFileResponse|Response|null
    {
        $data = $next($request);
        $responseTransformer = new ResponseTransformer();
        return $responseTransformer->handle($request, $data);
    }

}
