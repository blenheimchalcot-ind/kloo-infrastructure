<?php

return [
    \Kloo\Infrastructure\Constants\EnvironmentConstants::ENVIRONMENT_LOCAL => [
        "base_url" => config("app.localhost"),
        "services" => []
    ],
];