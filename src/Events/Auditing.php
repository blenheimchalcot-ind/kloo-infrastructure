<?php

// namespace Kloo\Infrastructure\Events;

// use Kloo\Infrastructure\Contracts\Auditable;
// use Kloo\Infrastructure\Contracts\AuditDriver;

// class Auditing
// {
//     /**
//      * The Auditable model.
//      *
//      * @var \Kloo\Infrastructure\Contracts\Auditable
//      */
//     public $model;

//     /**
//      * Audit driver.
//      *
//      * @var \Kloo\Infrastructure\Contracts\AuditDriver
//      */
//     public $driver;

//     /**
//      * Create a new Auditing event instance.
//      *
//      * @param \Kloo\Infrastructure\Contracts\Auditable   $model
//      * @param \Kloo\Infrastructure\Contracts\AuditDriver $driver
//      */
//     public function __construct(Auditable $model, AuditDriver $driver)
//     {
//         $this->model = $model;
//         $this->driver = $driver;
//     }
// }
