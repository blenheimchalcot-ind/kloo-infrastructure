<?php

namespace Kloo\Infrastructure\Events;

use Kloo\Infrastructure\Jobs\Traits\RetainBearerTokenTrait;

abstract class BaseQueueEvent extends BaseEvent
{
  use RetainBearerTokenTrait;

  protected function beforeDispatch(): void
  {
    parent::beforeDispatch();
    $this->retainBearerToken();
  }
}
