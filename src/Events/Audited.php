<?php

// namespace Kloo\Infrastructure\Events;

// use Kloo\Infrastructure\Contracts\Audit;
// use Kloo\Infrastructure\Contracts\Auditable;
// use Kloo\Infrastructure\Contracts\AuditDriver;

// class Audited
// {
//     /**
//      * The Auditable model.
//      *
//      * @var \Kloo\Infrastructure\Contracts\Auditable
//      */
//     public $model;

//     /**
//      * Audit driver.
//      *
//      * @var \Kloo\Infrastructure\Contracts\AuditDriver
//      */
//     public $driver;

//     /**
//      * The Audit model.
//      *
//      * @var \Kloo\Infrastructure\Contracts\Audit|null
//      */
//     public $audit;

//     /**
//      * Create a new Audited event instance.
//      *
//      * @param \Kloo\Infrastructure\Contracts\Auditable   $model
//      * @param \Kloo\Infrastructure\Contracts\AuditDriver $driver
//      * @param \Kloo\Infrastructure\Contracts\Audit       $audit
//      */
//     public function __construct(Auditable $model, AuditDriver $driver, Audit $audit = null)
//     {
//         $this->model = $model;
//         $this->driver = $driver;
//         $this->audit = $audit;
//     }
// }
