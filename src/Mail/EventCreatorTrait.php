<?php

namespace Kloo\Infrastructure\Mail;

use App\MicroAppServices\OrganizationMicroAppService;
use Kloo\Infrastructure\Exceptions\EmailEventCreatorListNotFoundException;
use Kloo\Infrastructure\Helpers\ArrayHelper;

trait EventCreatorTrait
{
    protected function fetchAndProcessEventCreatorList(?array $eventList, OrganizationMicroAppService $organizationMicroAppService): array|null
    {
        $eventCreatorList = [];
        if (ArrayHelper::isArrayValid($eventList)) {
            $creatorList = $this->fetchEventCreatorList($eventList, $organizationMicroAppService);
            $eventCreatorList = $this->iterateAndProcessEventCreatorList($creatorList);
        }
        return $eventCreatorList;
    }

    protected function iterateAndProcessEventCreatorList(?array $creatorList): array
    {
        $newCreatorList = [];
        if (ArrayHelper::isArrayValid($creatorList)) {
            foreach ($creatorList as  $value) {
                $newCreatorList[$value['id']] = $value;
            }
        }
        return $newCreatorList;
    }

    protected function fetchEventCreatorList(?array $eventList, OrganizationMicroAppService $organizationMicroAppService): ?array
    {
        $eventCreatorIds = $this->extractOutEventCreatorIdsFromEventList($eventList);
        $eventCreatorIds = array_unique($eventCreatorIds);
        $users = $organizationMicroAppService->getUserByUserOrgId($eventCreatorIds);
        if (!ArrayHelper::isArrayValid($users)) {
            throw new EmailEventCreatorListNotFoundException;
        }
        return $users;
    }

    /**
     * 
     *
     * @param array|null $eventList
     * @return array
     */
    protected function extractOutEventCreatorIdsFromEventList(?array $eventList): array
    {
        if (ArrayHelper::isArrayValid($eventList)) {
            $eventCreatorIds = array_column($eventList, "user_org_id");
            return $eventCreatorIds;
        }
        return [];
    }

    // protected function appendEventCreatorDetailsToEventItem(array $item): array
    // {
    //     $item['creator_details'] = $this->eventCreatorList[$item[$this->getCreatorUserOrgIdKeyNameFromEvent()]];
    //     return $item;
    // }
}
