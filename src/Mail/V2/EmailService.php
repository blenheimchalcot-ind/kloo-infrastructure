<?php

namespace Kloo\Infrastructure\Mail\V2;

use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Blade;
use Kloo\Infrastructure\Console\ContactMailable;
use Kloo\Infrastructure\Helpers\ArrayHelper;
use Kloo\Infrastructure\Models\Contact;
use Illuminate\Support\Facades\Mail;
use Kloo\Infrastructure\Exceptions\InternalServerErrorException;
use Kloo\Infrastructure\Models\EmailType;
use Kloo\Infrastructure\Facades\EmailServiceFacade;
use Kloo\Infrastructure\Facades\ExceptionReporterServiceFacade;
use Kloo\Infrastructure\Models\EmailOrgNotification;
use Kloo\Infrastructure\Facades\RequestSessionFacade;
use Kloo\Infrastructure\Repositories\EmailLogsRepository;
use Kloo\Infrastructure\Log\Logger;

class EmailService
{
    protected array $to;

    protected array $cc;

    protected array $bcc;

    protected string $subject;

    protected Mailable $mailable;

    public function __construct()
    {
        $this->init();
    }

    protected function init(): self
    {
        $this->to = [];
        $this->cc = [];
        $this->bcc = [];
        $this->subject = "";
        return $this;
    }

    public function setToEmail(array|string $to): self
    {
        $to = ArrayHelper::isArrayValid($to) ? $to : [$to];
        $this->to = $to;
        return $this;
    }

    public function setCCEmail(array|null|string $cc): self
    {
        $ccEmails = [];
        if ($cc) {
            $ccEmails = ArrayHelper::isArrayValid($cc) ? $cc : explode(",", $cc);
        }
        $this->cc = $ccEmails;
        return $this;
    }

    public function setBCCEmail(array|null|string $bcc): self
    {
        // $bcc = $bcc && ArrayHelper::isArrayValid($bcc) ? $bcc : explode(",", $bcc);
        $bccEmails = [];
        if ($bcc) {
            $bccEmails = ArrayHelper::isArrayValid($bcc) ? $bcc : explode(",", $bcc);
        }
        $this->bcc = $bccEmails;
        return $this;
    }

    public function setSubjectEmail(string $subject): EmailService
    {
        $this->subject = $subject;
        return $this;
    }

    public function setMailable(Mailable $mailable): EmailService
    {
        $this->mailable = $mailable;
        return $this;
    }

    public function sendEmail(): void
    {
        try {
            $this->mailable->subject($this->subject);
            Mail::to($this->to)->cc($this->cc)->bcc($this->bcc)->send($this->mailable);
            $this->init();
        } catch (\Throwable $th) {
            ExceptionReporterServiceFacade::report($th);
        }
    }
}
