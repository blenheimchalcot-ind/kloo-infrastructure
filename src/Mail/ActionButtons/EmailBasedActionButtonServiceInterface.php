<?php

namespace Kloo\Infrastructure\Mail\ActionButtons;

use Illuminate\Database\Eloquent\Collection;
use Kloo\Infrastructure\Models\EmailBasedAction;

interface EmailBasedActionButtonServiceInterface
{
    public function getEmailTypeAction(string $slugId): Collection;
    public function createEmailBasedAction(array $params): array;

    public function buildPayload(array $params): array;

    public function getButtonWrapperHtml(): string;

    public function encryptButton(array $emailData): array;
}
