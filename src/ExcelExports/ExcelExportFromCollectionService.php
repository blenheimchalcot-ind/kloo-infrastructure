<?php

namespace Kloo\Infrastructure\ExcelExports;

use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;

abstract class ExcelExportFromCollectionService extends BaseExcelExportService implements FromCollection
{
  public function __construct(Collection $data)
  {
    parent::__construct($data);
  }

  public function collection()
  {
    return $this->data;
  }
}
