<?php

namespace Kloo\Infrastructure\DTOS;

use Kloo\Infrastructure\Helpers\ArrayHelper;
use Kloo\Infrastructure\Helpers\UtilHelper;
use Kloo\Infrastructure\Traits\JsonSerialization;
use Spatie\DataTransferObject\DataTransferObject;

class BaseDTO extends DataTransferObject
{
    use JsonSerialization;

    public function convertKeysToSlug(): array
    {
        $props = $this->toArray();
        $newProps = [];
        if (ArrayHelper::isArrayValid($props)) {
            foreach ($props as $key => $val) {
                $newProps[UtilHelper::fromLowerCamelCaseToSnakeCase($key)] = $val;
            }
        }
        return $newProps;
    }
}
