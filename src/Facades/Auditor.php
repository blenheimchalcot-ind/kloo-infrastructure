<?php

namespace Kloo\Infrastructure\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @method static \Kloo\Infrastructure\Contracts\AuditDriver auditDriver(\Kloo\Infrastructure\Contracts\Auditable $model);
 * @method static void execute(\Kloo\Infrastructure\Contracts\Auditable $model);
 */
class Auditor extends Facade
{
    /**
     * {@inheritdoc}
     */
    protected static function getFacadeAccessor()
    {
        return \Kloo\Infrastructure\Auditor::class;
    }
}
