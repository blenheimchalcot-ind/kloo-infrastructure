<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Kloo\Infrastructure\Migrations\BaseTableMigration;

return new class extends BaseTableMigration
{
    protected string $tableName = "email_based_action_details";
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function upTable(string $tableName): void
    {
        Schema::create($tableName, function (Blueprint $table) {
            $table->uuid("id")->primary();
            $table->uuid("email_based_action_id");
            $table->string("action_by")->nullable();
            $table->dateTime('action_at')->nullable()->default(null);
            $table->string("action")->nullable();
            $table->string("action_system_data")->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('email_based_action_id')->references('id')->on('email_based_actions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_based_action_details');
    }
};
