<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('system_logs', function (Blueprint $table) {
            $table->uuid("user_name")->nullable();
            $table->uuid("organization_name")->nullable();
            $table->string("ms_name")->nullable();
            $table->string("api")->nullable();
            $table->longText("api_payload")->nullable();
            DB::statement("ALTER TABLE system_logs modify error_message longtext null");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('system_logs', function (Blueprint $table) {
            $table->dropColumn("user_name");
            $table->dropColumn("organization_name");
            $table->dropColumn("ms_name");
            $table->dropColumn("api");
            $table->dropColumn("api_payload");
        });
    }
};
