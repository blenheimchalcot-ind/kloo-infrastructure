<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;


return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::table('search_filter_configs', function ($table) {
            $table->tinyInteger('sequence_filter_by')->nullable()->after('search_column');
        });
    }

    public function down()
    {
        Schema::table('search_filter_configs', function ($table) {
            $table->drop('sequence_filter_by');
        });
    }
};