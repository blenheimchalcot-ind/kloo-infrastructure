<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use \Kloo\Infrastructure\Migrations\BaseTableMigration;

class addColumnBccEmailsToEmailTypesTable extends \Kloo\Infrastructure\Migrations\BaseColumnMigration
{
    protected string $tableName = "email_types";

    protected string $columnName = "bcc_emails";

    public function upColumn(string $tableName, string $columnName): void
    {
        Schema::table($tableName, function (Blueprint $table) use ($columnName) {
            $table->longText($columnName)->nullable();
            $table->longText('cc_emails')->nullable();
        });
    }

    public function downColumn(string $tableName, string $columnName): void
    {
        Schema::table($tableName, function (Blueprint $table) use ($columnName) {

        });
    }
}