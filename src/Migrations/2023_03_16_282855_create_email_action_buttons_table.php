<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Kloo\Infrastructure\Constants\EntityStatusConstant;
use Kloo\Infrastructure\Migrations\BaseTableMigration;

return new class extends BaseTableMigration
{
    protected string $tableName = "email_action_buttons";
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function upTable(string $tableName): void
    {
        Schema::create($tableName, function (Blueprint $table) {
            $table->uuid("id")->primary();
            $table->string("type")->nullable();
            $table->enum("status", [EntityStatusConstant::ACTIVE, EntityStatusConstant::INACTIVE])->default(EntityStatusConstant::ACTIVE);
            $table->string("order_btn_by")->nullable();
            $table->text("body")->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_action_buttons');
    }
};
