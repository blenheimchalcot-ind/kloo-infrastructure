<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Kloo\Infrastructure\Constants\EntityStatusConstant;
use Kloo\Infrastructure\Migrations\BaseTableMigration;

return new class extends BaseTableMigration
{
    protected string $tableName = "email_type_actions";
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function upTable(string $tableName): void
    {
        Schema::create($tableName, function (Blueprint $table) {
            $table->uuid("id")->primary();
            $table->uuid("email_type_id");
            $table->uuid("email_action_button_id");
            $table->enum("status", [EntityStatusConstant::ACTIVE, EntityStatusConstant::INACTIVE])->default(EntityStatusConstant::ACTIVE);
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('email_type_id')->references('id')->on('email_types');
            $table->foreign('email_action_button_id')->references('id')->on('email_action_buttons');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_type_actions');
    }
};
