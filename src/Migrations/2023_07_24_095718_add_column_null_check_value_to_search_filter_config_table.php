<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{

    public function up()
    {
        Schema::table('search_filter_configs', function ($table) {
            $table->string('null_check_value')->nullable()->after('role');
        });
    }

    public function down()
    {
        Schema::table('search_filter_configs', function ($table) {
            $table->drop('null_check_value');
        });
    }
};