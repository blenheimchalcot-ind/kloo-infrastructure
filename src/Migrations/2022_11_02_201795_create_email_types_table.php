<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use \Kloo\Infrastructure\Migrations\BaseTableMigration;

class CreateEmailTypesTable extends BaseTableMigration
{
    protected string $tableName = "email_types";

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function upTable(string $tableName): void
    {
        Schema::create($tableName, function (Blueprint $table) {
            $table->uuid("id")->primary();
            $table->string("slug")->nullable();
            $table->string("event")->nullable();
            $table->enum("status",['active','inactive'])->default('inactive')->nullable();
            $table->string("belongs_to")->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

}