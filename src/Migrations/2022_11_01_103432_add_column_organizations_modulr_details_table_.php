<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use \Kloo\Infrastructure\Migrations\BaseTableMigration;

class AddColumnOrganizationsModulrDetailsTable extends \Kloo\Infrastructure\Migrations\BaseColumnMigration
{
    protected string $tableName = "organizations_modulr_details";

    protected string $columnName = "modulr_auth_secret";

    public function upColumn(string $tableName, string $columnName): void
    {
        Schema::table($tableName, function (Blueprint $table) use ($columnName) {
            $table->longText($columnName)->nullable();
            DB::statement("alter table organizations_modulr_details modify column modulr_auth_key longtext null;");
        });
    }

    public function downColumn(string $tableName, string $columnName): void
    {
        Schema::table($tableName, function (Blueprint $table) use ($columnName) {
            $table->dropColumn($columnName);
        });
    }
}