<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use \Kloo\Infrastructure\Migrations\BaseTableMigration;

class CreateEmailNotificationsTable extends BaseTableMigration
{
    protected string $tableName = "email_org_notifications";

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function upTable(string $tableName): void
    {
        Schema::create($tableName, function (Blueprint $table) {
            $table->uuid("id")->primary();
            $table->string("email_slug_id");
            $table->uuid('organization_id');
            $table->enum("is_enabled",['enabled','disabled'])->default('disabled')->nullable();
            $table->string("subject")->nullable();
            $table->string("event_description")->nullable();
            $table->enum("is_audit",['true','false'])->default('false')->nullable();
            $table->longText("content")->nullable();
            $table->enum("is_approval_process",['true','false'])->default('false')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

}