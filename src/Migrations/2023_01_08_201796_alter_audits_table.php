<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;


return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::table('audits', function ($table) {
            $table->uuid('request_id')->nullable()->after('user_id');
            //$table->uuid('primary_id')->nullable()->after('request_id')->index('primary_id');
            $table->uuid('organisation_id')->nullable();
            $table->uuid('user_org_id')->nullable()->after('organisation_id');
            $table->string('sub_event_name')->nullable();
            $table->longText("custom_values")->nullable();
            $table->foreign('organisation_id')
                ->references('id')
                ->on('organizations')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('user_org_id')
                ->references('id')
                ->on('user_organization_details')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */

    public function down()
    {
        //
    }
};