<?php

use App\Constants\PurchaseOrderStatus;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Kloo\Infrastructure\Constants\ModuleConstants;
use Kloo\Infrastructure\Constants\EmailActionConstant;
use Kloo\Infrastructure\Migrations\BaseTableMigration;

return new class extends BaseTableMigration
{
    protected string $tableName = "email_based_actions";
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function upTable(string $tableName): void
    {
        Schema::create($tableName, function (Blueprint $table) {
            $table->uuid("id")->primary();
            $table->enum("module_belongs_to", [ModuleConstants::CARDS , ModuleConstants::ACCOUNTS_PAYABLE, ModuleConstants::ACCOUNTS_INTEGRATION, ModuleConstants::CARD_EXPENSE,ModuleConstants::AUTH,ModuleConstants::ORGANISATION,ModuleConstants::SMART_APPROVAL,ModuleConstants::TEAM,ModuleConstants::UACL,ModuleConstants::USER,ModuleConstants::PURCHASE_ORDERS]);
            $table->string("token")->unique();
            $table->json("request_data")->nullable();
            $table->enum("is_actioned", [EmailActionConstant::PENDING,EmailActionConstant::COMPLETED,EmailActionConstant::EXPIRED,EmailActionConstant::INVALID])->default(EmailActionConstant::PENDING);
            $table->string("request_id")->nullable();
            $table->string("workflow_id")->nullable();
            $table->text("callback_url")->nullable();
            $table->enum("is_actioned_for", ['platform','email'])->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_based_actions');
    }
};
