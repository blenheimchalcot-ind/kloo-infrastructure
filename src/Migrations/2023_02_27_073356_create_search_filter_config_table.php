<?php


use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Kloo\Infrastructure\Constants\SearchFilterDataTypeConstant;
use Kloo\Infrastructure\Constants\SearchFilterOperatorTypeConstant;
use Kloo\Infrastructure\Migrations\BaseTableMigration;

return new class extends BaseTableMigration
{
    protected string $tableName = "search_filter_configs";
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function upTable(string $tableName): void
    {
        Schema::create($tableName, function (Blueprint $table) {
            $table->uuid("id")->primary();
            $table->string("name");
            $table->string("field_name");
            $table->string("join_field_name", 255)->nullable();
            $table->string("label");
            $table->tinyInteger("is_searchable")->default(1);
            $table->enum("type",["all","filter","search"]);
            $table->enum("operator", [
                SearchFilterOperatorTypeConstant::OPERATOR_TYPE_EQUAL, SearchFilterOperatorTypeConstant::OPERATOR_TYPE_LIKE, SearchFilterOperatorTypeConstant::OPERATOR_TYPE_BETWEEN, SearchFilterOperatorTypeConstant::OPERATOR_TYPE_WHEREIN, SearchFilterOperatorTypeConstant::OPERATOR_TYPE_GREATER_THAN, SearchFilterOperatorTypeConstant::OPERATOR_TYPE_GREATER_THAN_EQUALTO, SearchFilterOperatorTypeConstant::OPERATOR_TYPE_LESS_THAN, SearchFilterOperatorTypeConstant::OPERATOR_TYPE_LESS_THAN_EQUAL_TO
            ]);
            $table->enum("data_type", [
                SearchFilterDataTypeConstant::SEARCH_FILTER_TYPE_STRING,
                SearchFilterDataTypeConstant::SEARCH_FILTER_TYPE_NUMBER,
                SearchFilterDataTypeConstant::SEARCH_FILTER_TYPE_DATE,
                SearchFilterDataTypeConstant::SEARCH_FILTER_TYPE_DATETIME,
                SearchFilterDataTypeConstant::SEARCH_FILTER_TYPE_ARRAY
            ]);
            $table->string("filter_data_keys")->nullable();
            $table->string("search_column",64)->nullable();
            $table->timestamps();
        });
    }
};
