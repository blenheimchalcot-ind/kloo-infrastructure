<?php

namespace Kloo\Infrastructure\Services;

use Kloo\Infrastructure\Constants\EnvironmentConstants;
use Kloo\Infrastructure\Helpers\ArrayHelper;
use Kloo\Infrastructure\Helpers\EnvironmentHelper;
use Kloo\Infrastructure\Log\Logger;
use Kloo\Infrastructure\Repositories\EmailLogsRepository;

class EmailLogService extends BaseService
{
    protected $emailLogsRepository;

    public function __construct(EmailLogsRepository $emailLogsRepository)
    {
        $this->emailLogsRepository = $emailLogsRepository;
    }

    public function createEmailLog($userId,$action,$link,$emailData)
    {
        return $this->emailLogsRepository->createEmailLog($userId,$action,$link,$emailData);
    }
}