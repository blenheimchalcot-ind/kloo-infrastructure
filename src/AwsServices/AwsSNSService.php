<?php

namespace Kloo\Infrastructure\AwsServices;

use Illuminate\Support\Str;
use Kloo\Infrastructure\Exceptions\PublishTopicFailedException;
use Kloo\Infrastructure\Log\SessionLogger;

class AwsSNSService extends AwsCommonSNSService
{
    /**
     * that is going to publish the sns to Topic created specially for Kloo PubSub
     *
     * @param string $topicName
     * @param array $message
     * @return void
     */
    public function publish(string $topicName, array $message): ?array
    {

        SessionLogger::start(__METHOD__, ['topic' => $topicName, 'message' => $message]);
        $message['token'] = request()->header("Authorization");
        $message['unique_message_id'] = Str::uuid()->toString();
        $topicName = $this->buildTopicWithEnv($topicName);
        $result = $this->process($topicName, $message);
        SessionLogger::end(__METHOD__, ['topic' => $topicName, 'message' => $message]);
        return $result?->toArray();
    }

    public function handleSDKException(\Throwable $throwable, ?array $params = null): void
    {
        throw new PublishTopicFailedException($throwable, $params);
    }
}
