<?php

namespace Kloo\Infrastructure\Traits;

use Illuminate\Support\Collection;
use Kloo\Infrastructure\DTOS\FetchNMergeResponseToCollectionDTO;
use Kloo\Infrastructure\DTOS\MergeResponseToCollectionDTO;

trait MicroServiceAppAllResponseMerger
{
    use MicroServiceAppResponseMerger;
    public function fetchAllNMergeResponseToCollection(FetchNMergeResponseToCollectionDTO $collectionDTO): ?Collection
    {
        $response = $this->fetchResponseByCollection($collectionDTO->collection, $collectionDTO->collectionKey, $collectionDTO->mergeKeyName, $collectionDTO->recursiveCollectionKey, $collectionDTO->callable);
        return $this->mergeResponseToCollection(MergeResponseToCollectionDTO::fromFetchNMergeResponseToCollectionDTO($collectionDTO, $response), $collectionDTO->recursiveCollectionKey);
    }
}
