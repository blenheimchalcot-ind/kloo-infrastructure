<?php

namespace Kloo\Infrastructure\Traits;

trait MergeRequestParamForValidation
{
    public function validationData()
    {
        return $this->route()->parameters() + $this->all();
    }
}