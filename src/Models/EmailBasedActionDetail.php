<?php

namespace Kloo\Infrastructure\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kloo\Infrastructure\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmailBasedActionDetail extends BaseModel
{
    use HasFactory, SoftDeletes;
    protected $guarded = [];

    protected $table = "email_based_action_details";
}
