<?php

namespace Kloo\Infrastructure\Models;

use Kloo\Infrastructure\Models\BaseModel;
use Kloo\Infrastructure\Scopes\RoleClauseScope;

class NotificationsLogs extends BaseModel
{
    protected $table = 'push_notification_logs';

    protected $fillable = ["fcm_token","response"];
}