<?php

namespace Kloo\Infrastructure\Models;

use Illuminate\Database\Eloquent\Builder;
use Kloo\Infrastructure\Facades\RequestSessionFacade;
use Kloo\Infrastructure\Models\BaseModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class EmailType extends BaseModel
{
    use HasFactory;

    protected $fillable = ["id","slug", "event", "status", "belongs_to"];

}
