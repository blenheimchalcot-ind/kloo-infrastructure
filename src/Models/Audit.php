<?php

namespace Kloo\Infrastructure\Models;

use Kloo\Infrastructure\Traits\ApplyAuditFilterTrait;
use Illuminate\Database\Eloquent\Model;

class Audit extends \OwenIt\Auditing\Models\Audit
{
    use ApplyAuditFilterTrait;
}
