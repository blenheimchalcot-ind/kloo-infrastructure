<?php

namespace Kloo\Infrastructure\Models;

use Kloo\Infrastructure\Traits\ApplyAuditFilterTrait;

class SearchFilterConfig extends BaseModel
{
  // use ApplyAuditFilterTrait;

  protected $casts = ['is_searchable' => "boolean"];
}
