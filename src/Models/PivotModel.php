<?php

namespace Kloo\Infrastructure\Models;

use Kloo\Infrastructure\Models\BaseModel;

class PivotModel extends BaseModel
{
  public function newEloquentBuilder($query)
  {
    return new PivotModelEagerLoadBuilder($query);
  }
}
