<?php

namespace Kloo\Infrastructure\PubSubProcessors;

abstract class BasePubSubProcessor
{
  public abstract function process($params);
}
