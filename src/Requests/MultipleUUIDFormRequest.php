<?php

namespace Kloo\Infrastructure\Requests;

use Kloo\Infrastructure\Rules\MultipleUUIDRule;
use Kloo\Infrastructure\Traits\MergeRequestParamForValidation;

class MultipleUUIDFormRequest extends BaseFormRequest
{
    use MergeRequestParamForValidation;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            "id" => [new MultipleUUIDRule()],
        ];
    }
}