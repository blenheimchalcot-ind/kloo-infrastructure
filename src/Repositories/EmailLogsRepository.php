<?php /** @noinspection PhpUndefinedMethodInspection */

namespace Kloo\Infrastructure\Repositories;

use Kloo\Infrastructure\Models\EmailLog;
use Kloo\Infrastructure\Log\Logger;
use App\DTOs\UpdateAddressDTO;
use Kloo\Infrastructure\Repositories\BaseRepository;
use Kloo\Infrastructure\Exceptions\BadRequestException;

class EmailLogsRepository extends BaseRepository
{
    public function createEmailLog(string $userId,string $action,string $link,array $emailData)
    {
        return  EmailLog::create(
                    [
                        'user_id' => $userId,
                        'action' => $action,
                        'link' => $link,
                        'payload' => json_encode($emailData),
                        'email_sent_at' => now()
                    ]
                );    
    }
}
