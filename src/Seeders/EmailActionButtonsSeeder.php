<?php

namespace Kloo\Infrastructure\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Kloo\Infrastructure\Models\EmailActionButton;

class EmailActionButtonsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $approve = '<td style="height:16px" height="16"></td>
        </tr>
        <tr>
           <td>
              <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; text-align: center; color: #000000; padding-top:0px; padding-right:0px; padding-bottom:0px; padding-left:0px; margin-top:0px; margin-right:auto; margin-bottom:0px; margin-left:auto;width: 90%;" width="90%">
        <tbody>
           <tr>
              <td style="width:40%">
                 <a href="{approve_btn_link}" style="text-decoration:none; color:#ffffff;mso-height-rule: exactly;display: block;" target="_blank">
                    <table border="0" cellspacing="0" cellpadding="0" align="left" style="width:95%;">
                       <tbody>
                          <tr>
                             <td style="padding: 12px 20px 12px 20px; border-radius:8px; background-color: #001B37; color: #FFFFFF; text-decoration: none; font-weight:600;" align="center">
                 <a href="{approve_btn_link}" style="text-decoration:none; color:#ffffff;mso-height-rule: exactly;display: block;" target="_blank"> <span style="font-size: 16px; line-height: 24px; font-family: \'Inter\', sans-serif; font-weight: 600; color: #FFFFFF; display: inline-block; text-decoration: none;">Approve</span> </a> </td> </tr> </tbody> </table> </a>
        </td>';

        $reject = '<td style="width:40%">
        <a href="{reject_btn_link}" style="text-decoration:none; color:#ce0000;mso-height-rule: exactly;display: block;" target="_blank">
        <table border="0" cellspacing="0" cellpadding="0" align="right" style="width: 95%;">
          <tbody>
            <tr>
              <td style="padding: 12px 20px 12px 20px; border-radius:8px; background-color: #FFFFFF; color: #CE0000; text-decoration: none; font-weight: 600; border:solid 1px #CE0000;"
                align="center">
                <a href="{reject_btn_link}" style="text-decoration:none; color:#ce0000;mso-height-rule: exactly;display: block;" target="_blank">
                  <span style="font-size: 16px;  line-height: 24px; font-family: \'Inter\', sans-serif; font-weight: 600; color: #CE0000;  display: inline-block; text-decoration: none;">Reject</span>
                </a>
              </td>
            </tr>
          </tbody>
        </table>
        </a>
        </td>
        </tr>
        </tbody>
        </table>
        </td>
        </tr>';

        $view = '<tr>
        <td style="height:24px" height="24"></td>
        </tr>
        <tr>
            <td style="padding-top: 0; padding-right:0; padding-bottom:0; padding-left:0; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0; text-align: center;">
                <p style="font-size: 14px; line-height: 21px; color:#001b37;  font-family: \'Poppins\', sans-serif; font-weight: 600;font-style: normal;margin:0; padding:0;">
                   <a href="{view_btn_link}" style="font-size: 14px; line-height: 21px; color:#001b37;  font-family: \'Poppins\', sans-serif; font-weight: 600;font-style: normal;margin:0; padding:0; text-decoration: none;">View
                                    on platform</a>
                </p>
            </td>
        </tr>';

        $pay_now = '<button style="background-color:blue;color:white;">Pay Now</button>';

        $actionButtons =
        [
            [
                'id' => Str::uuid()->toString(),
                'type' => 'approve',
                'status' => 'active',
                'order_btn_by' => 1,
                'body' => $approve
            ],
            [
                'id' => Str::uuid()->toString(),
                'type' => 'reject',
                'status' => 'active',
                'order_btn_by' => 2,
                'body' => $reject
            ],
            [
                'id' => Str::uuid()->toString(),
                'type' => 'view',
                'status' => 'active',
                'order_btn_by' => 3,
                'body' => $view
            ],
            [
                'id' => Str::uuid()->toString(),
                'type' => 'pay_now',
                'status' => 'active',
                'order_btn_by' => 4,
                'body' => $pay_now
            ],
        ];

        foreach ($actionButtons as $button) {
            // $type = EmailActionButton::where('type', $button['type'])->get();
            // if (count($type) == 0) {
            //     EmailActionButton::create($button);
            // }
            $type = EmailActionButton::where('type', $button['type'])->first();
            if (empty($type)) {
                $button['id'] = Str::uuid()->toString();
                EmailActionButton::create($button);
            }else{
                $type->update([
                    "status" => $button['status'],
                    "order_btn_by" => $button['order_btn_by'],
                    "body" => $button['body']
                ]);
            }
        }
    }
}
