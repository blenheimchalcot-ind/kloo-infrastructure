<?php

namespace Kloo\Infrastructure\Seeders;

use Illuminate\Database\Seeder;
use Kloo\Infrastructure\Models\EmailType;
use Kloo\Infrastructure\Models\EmailOrgNotification;
use Illuminate\Support\Str;
use DB;

class EmailOrgNotificationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EmailOrgNotification::disableAuditing();
        $transactionDeclinedContent = '<tr>
        <td style="width:100%;">
            <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation"
                style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; width:100%; text-align: left;">
                <tr>
                    <td
                        style="padding:8px 0px; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0;">
                        <p
                            style="font-size: 14px; line-height: normal; color:#000000;  font-family: \'Poppins\', sans-serif; font-weight: 400;font-style: normal;margin:0; padding:0;">
                            Organisation: <span style="font-weight: 600;">{organization_name}</span>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td
                        style="padding:8px 0px; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0; background-color: #ffffff;">
                        <p
                            style="font-size: 14px; line-height: normal; color:#000000;  font-family: \'Poppins\', sans-serif; font-weight: 400;font-style: normal;margin:0; padding:0;">
                            Merchant: <span style="font-weight: 600;">{vendor}</span>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td
                        style="padding:8px 0px; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0;">
                        <p
                            style="font-size: 14px; line-height: normal;  color:#000000;  font-family: \'Poppins\', sans-serif; font-weight: 400;font-style: normal;margin:0; padding:0;">
                            Amount: <span style="font-weight: 600;">{amount} {currency}</span>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td
                        style="padding:8px 0px; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0; background-color: #ffffff;">
                        <p
                            style="font-size: 14px; line-height: normal;  color:#000000;  font-family: \'Poppins\', sans-serif; font-weight: 400;font-style: normal;margin:0; padding:0;">
                            Date: <span style="font-weight: 600;">{date}</span>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td
                        style="padding:8px 0px; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0;">
                        <p
                            style="font-size: 14px; line-height: normal;  color:#000000;  font-family: \'Poppins\', sans-serif; font-weight: 400;font-style: normal;margin:0; padding:0;">
                            Time: <span style="font-weight: 600;">{time}</span>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td
                        style="padding:8px 0px; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0; background-color: #ffffff;">
                        <p
                            style="font-size: 14px; line-height: normal; color:#000000;  font-family: \'Poppins\', sans-serif; font-weight: 400;font-style: normal;margin:0; padding:0;">
                            Cardholder name: <span style="font-weight: 600;">{card_name}</span>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td
                        style="padding:8px 0px; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0; background-color: #ffffff;">
                        <p
                            style="font-size: 14px; line-height: normal; color:#000000;  font-family: \'Poppins\', sans-serif; font-weight: 400;font-style: normal;margin:0; padding:0;">
                            Card number: <span style="font-weight: 600;">{card_number}</span>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td
                        style="padding:8px 0px; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0; background-color: #ffffff;">
                        <p
                            style="font-size: 14px; line-height: normal; color:#000000;  font-family: \'Poppins\', sans-serif; font-weight: 400;font-style: normal;margin:0; padding:0;">
                            Card type: <span style="font-weight: 600;">{card_type}</span>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td style="height:16px" height="16"></td>
                    <td style="height:16px" height="16"></td>
                </tr>
            </table>
        </td>
        </tr>
        <tr>
        <td style="width:100%;">
            <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation"
                style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; width:100%; text-align: left;">
                <tr>
                    <td
                        style="padding-top: 0; padding-right:0; padding-bottom:0; padding-left:0; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0;">
                        <p
                            style="font-size: 14px; line-height: normal; color:#000000;  font-family: \'Poppins\', sans-serif; font-weight: 400;font-style: normal;margin:0; padding:0;">
                            Decline reason: {decline_reason}
                        </p>
                    </td>
                </tr>
                <tr>
                    <td style="height:16px" height="16"></td>
                </tr>
                <tr>
                    <td style="height:35px" height="35"></td>
                </tr>
                <tr>
                    <td
                        style="padding-top: 0; padding-right:0; padding-bottom:0; padding-left:0; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0;">
                        <p
                            style="font-size: 14px; line-height: normal; color:#000000;  font-family: \'Poppins\', sans-serif; font-weight: 600;font-style: normal;margin:0; padding:0;">
                            The Kloo Team
                        </p>
                    </td>
                </tr>
                <tr>
                    <td style="height:30px" height="30"></td>
                </tr>
            </table>
        </td>
        </tr>';

        $modulrTransactionDeclinedContent = '<tr>
        <td style="width:100%;">
            <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation"
                style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; width:100%; text-align: left;">
                <tr>
                    <td
                        style="padding:8px 0px; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0;">
                        <p
                            style="font-size: 14px; line-height: normal; color:#000000;  font-family: \'Poppins\', sans-serif; font-weight: 400;font-style: normal;margin:0; padding:0;">
                            Organisation: <span style="font-weight: 600;">{organization_name}</span>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td
                        style="padding:8px 0px; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0; background-color: #ffffff;">
                        <p
                            style="font-size: 14px; line-height: normal; color:#000000;  font-family: \'Poppins\', sans-serif; font-weight: 400;font-style: normal;margin:0; padding:0;">
                            Merchant: <span style="font-weight: 600;">{vendor}</span>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td
                        style="padding:8px 0px; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0;">
                        <p
                            style="font-size: 14px; line-height: normal;  color:#000000;  font-family: \'Poppins\', sans-serif; font-weight: 400;font-style: normal;margin:0; padding:0;">
                            Amount: <span style="font-weight: 600;">{amount} {currency}</span>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td
                        style="padding:8px 0px; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0; background-color: #ffffff;">
                        <p
                            style="font-size: 14px; line-height: normal;  color:#000000;  font-family: \'Poppins\', sans-serif; font-weight: 400;font-style: normal;margin:0; padding:0;">
                            Date: <span style="font-weight: 600;">{date}</span>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td
                        style="padding:8px 0px; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0;">
                        <p
                            style="font-size: 14px; line-height: normal;  color:#000000;  font-family: \'Poppins\', sans-serif; font-weight: 400;font-style: normal;margin:0; padding:0;">
                            Time: <span style="font-weight: 600;">{time}</span>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td
                        style="padding:8px 0px; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0; background-color: #ffffff;">
                        <p
                            style="font-size: 14px; line-height: normal; color:#000000;  font-family: \'Poppins\', sans-serif; font-weight: 400;font-style: normal;margin:0; padding:0;">
                            Cardholder name: <span style="font-weight: 600;">{card_name}</span>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td
                        style="padding:8px 0px; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0; background-color: #ffffff;">
                        <p
                            style="font-size: 14px; line-height: normal; color:#000000;  font-family: \'Poppins\', sans-serif; font-weight: 400;font-style: normal;margin:0; padding:0;">
                            Card number: <span style="font-weight: 600;">{card_number}</span>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td
                        style="padding:8px 0px; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0; background-color: #ffffff;">
                        <p
                            style="font-size: 14px; line-height: normal; color:#000000;  font-family: \'Poppins\', sans-serif; font-weight: 400;font-style: normal;margin:0; padding:0;">
                            Card type: <span style="font-weight: 600;">{card_type}</span>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td style="height:16px" height="16"></td>
                    <td style="height:16px" height="16"></td>
                </tr>
            </table>
        </td>
        </tr>
        <tr>
        <td style="width:100%;">
            <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation"
                style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; width:100%; text-align: left;">
                <tr>
                    <td
                        style="padding-top: 0; padding-right:0; padding-bottom:0; padding-left:0; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0;">
                        <p
                            style="font-size: 14px; line-height: normal; color:#000000;  font-family: \'Poppins\', sans-serif; font-weight: 400;font-style: normal;margin:0; padding:0;">
                            Decline reason: {decline_reason}
                        </p>
                    </td>
                </tr>
                <tr>
                    <td
                        style="padding-top: 8px; padding-right:0; padding-bottom:0; padding-left:0; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0;">
                        <p
                            style="font-size: 14px; line-height: normal; color:#000000;  font-family: \'Poppins\', sans-serif; font-weight: 400;font-style: normal;margin:0; padding-top:8px;">
                            Modulr Decline reason: {modulr_reason}
                        </p>
                    </td>
                </tr>
                <tr>
                    <td style="height:16px" height="16"></td>
                </tr>
                <tr>
                    <td style="height:35px" height="35"></td>
                </tr>
                <tr>
                    <td
                        style="padding-top: 0; padding-right:0; padding-bottom:0; padding-left:0; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0;">
                        <p
                            style="font-size: 14px; line-height: normal; color:#000000;  font-family: \'Poppins\', sans-serif; font-weight: 600;font-style: normal;margin:0; padding:0;">
                            The Kloo Team
                        </p>
                    </td>
                </tr>
                <tr>
                    <td style="height:30px" height="30"></td>
                </tr>
            </table>
        </td>
        </tr>';


        $invoice_approval_content = '<tr>
        <td style="width:100%;">
           <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation"
              style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; width:100%; text-align: left;">
              <tr>
                 <td
                    style="padding:10px; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0; background-color: #f3f4ff; width:40%;">
                    <p
                       style="font-size: 12px; line-height: 18px; color:#000000;  font-family: \'Poppins\', sans-serif; font-weight: 400;font-style: normal;margin:0; padding:0;">
                       Invoice amount
                    </p>
                 </td>
                 <td
                    style="padding:10px; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0; background-color: #f3f4ff;">
                    <p
                       style="font-size: 12px; line-height: 18px; color:#000000;  font-family: \'Poppins\', sans-serif; font-weight: 600;font-style: normal;margin:0; padding:0;">
                       {amount}
                    </p>
                 </td>
              </tr>
              <tr>
                 <td
                    style="padding:10px; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0; background-color: #ffffff; width:40%;">
                    <p
                       style="font-size: 12px; line-height: 18px; color:#000000;  font-family: \'Poppins\', sans-serif; font-weight: 400;font-style: normal;margin:0; padding:0;">
                       Invoice number
                    </p>
                 </td>
                 <td
                    style="padding:10px; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0; background-color: #ffffff;">
                    <p
                       style="font-size: 12px; line-height: 18px; color:#000000;  font-family: \'Poppins\', sans-serif; font-weight: 600;font-style: normal;margin:0; padding:0;">
                       {invoice_number}
                    </p>
                 </td>
              </tr>
              <tr>
                 <td
                    style="padding:10px; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0; background-color: #f3f4ff; width:40%;">
                    <p
                       style="font-size: 12px; line-height: 18px; color:#000000;  font-family: \'Poppins\', sans-serif; font-weight: 400;font-style: normal;margin:0; padding:0;">
                       Invoice date
                    </p>
                 </td>
                 <td
                    style="padding:10px; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0; background-color: #f3f4ff;">
                    <p
                       style="font-size: 12px; line-height: 18px; color:#000000;  font-family: \'Poppins\', sans-serif; font-weight: 600;font-style: normal;margin:0; padding:0;">
                       {invoice_date}
                    </p>
                 </td>
              </tr>
              <tr>
                 <td
                    style="padding:10px; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0; background-color: #ffffff; width:40%;">
                    <p
                       style="font-size: 12px; line-height: 18px; color:#000000;  font-family: \'Poppins\', sans-serif; font-weight: 400;font-style: normal;margin:0; padding:0;">
                       Due date
                    </p>
                 </td>
                 <td
                    style="padding:10px; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0; background-color: #ffffff;">
                    <p
                       style="font-size: 12px; line-height: 18px; color:#000000;  font-family: \'Poppins\', sans-serif; font-weight: 600;font-style: normal;margin:0; padding:0;">
                       {invoice_due_date}
                    </p>
                 </td>
              </tr>
              <tr>
                 <td
                    style="padding:10px; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0; background-color: #f3f4ff; width:40%;">
                    <p
                       style="font-size: 12px; line-height: 18px; color:#000000;  font-family: \'Poppins\', sans-serif; font-weight: 400;font-style: normal;margin:0; padding:0;">
                       Supplier
                    </p>
                 </td>
                 <td
                    style="padding:10px; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0; background-color: #f3f4ff;">
                    <p
                       style="font-size: 12px; line-height: 18px; color:#000000;  font-family: \'Poppins\', sans-serif; font-weight: 600;font-style: normal;margin:0; padding:0;">
                       {payee}
                    </p>
                 </td>
              </tr>
              <tr>
                 <td
                    style="padding:10px; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0; background-color: #ffffff; width:40%;">
                    <p
                       style="font-size: 12px; line-height: 18px; color:#000000;  font-family: \'Poppins\', sans-serif; font-weight: 400;font-style: normal;margin:0; padding:0;">
                       Description
                    </p>
                 </td>
                 <td
                    style="padding:10px; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0; background-color: #ffffff;">
                    <p
                       style="font-size: 12px; line-height: 18px; color:#000000;  font-family: \'Poppins\', sans-serif; font-weight: 600;font-style: normal;margin:0; padding:0;">
                       {description}
                    </p>
                 </td>
              </tr>
              <tr>
                 <td style="height:16px" height="16"></td>
                 <td style="height:16px" height="16"></td>
              </tr>
           </table>
        </td>
        </tr>';

        $confirmation_email_based_action_content = '<tr>
        <td
          style="
            padding-top: 0;
            padding-right: 0;
            padding-bottom: 0;
            padding-left: 0;
            margin-top: 0;
            margin-right: 0;
            margin-bottom: 0;
            margin-left: 0;
            text-align:left;
          "
        >
          <p
            style="
              font-size: 14px;
              line-height: 21px;
              color: #000000;
              font-family: \'Poppins\', sans-serif;
              font-weight: 400;
              font-style: normal;
              margin: 0;
              padding: 0;
            "
          >
          You have successfully {invoice_status} invoice {invoice_number} {payee} submitted by {submitters_name}.
          </p><br><br>
        </td>
      </tr>
      <tr>
        <td
          style="
            padding-top: 0;
            padding-right: 0;
            padding-bottom: 0;
            padding-left: 0;
            margin-top: 0;
            margin-right: 0;
            margin-bottom: 0;
            margin-left: 0;
            text-align:left;
          "
        >
          <p
            style="
              font-size: 14px;
              line-height: 21px;
              color: #000000;
              font-family: \'Poppins\', sans-serif;
              font-weight: 400;
              font-style: normal;
              margin: 0;
              padding: 0;
            "
          >
            No further action is required at this stage and this
            action has been reflected your organisations Kloo
            account.
          </p>
        </td>
      </tr>';

      $po_approval_content = '<tr>
        <td style="width:100%;">
            <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; width:100%; text-align: left;">
                <tr>
                <td style="padding:10px; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0; background-color: #f3f4ff; width:40%;">
                    <p style="font-size: 12px; line-height: 18px; color:#000000; font-family: \'Poppins\', sans-serif; font-weight: 400;font-style: normal;margin:0; padding:0;"> Supplier </p>
                </td>
                <td style="padding:10px; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0; background-color: #f3f4ff;">
                    <p style="font-size: 12px; line-height: 18px; color:#000000; font-family: \'Poppins\', sans-serif; font-weight: 600;font-style: normal;margin:0; padding:0;"> {payee} </p>
                </td>
                </tr>
                <tr>
                <td style="padding:10px; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0; background-color: #ffffff; width:40%;">
                    <p style="font-size: 12px; line-height: 18px; color:#000000; font-family: \'Poppins\', sans-serif; font-weight: 400;font-style: normal;margin:0; padding:0;"> Description </p>
                </td>
                <td style="padding:10px; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0; background-color: #ffffff;">
                    <p style="font-size: 12px; line-height: 18px; color:#000000; font-family: \'Poppins\', sans-serif; font-weight: 600;font-style: normal;margin:0; padding:0;"> {description} </p>
                </td>
                </tr>
                <tr>
                <td style="padding:10px; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0; background-color: #f3f4ff; width:40%;">
                    <p style="font-size: 12px; line-height: 18px; color:#000000; font-family: \'Poppins\', sans-serif; font-weight: 400;font-style: normal;margin:0; padding:0;"> Owner </p>
                </td>
                <td style="padding:10px; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0; background-color: #f3f4ff;">
                    <p style="font-size: 12px; line-height: 18px; color:#000000; font-family: \'Poppins\', sans-serif; font-weight: 600;font-style: normal;margin:0; padding:0;"> {owner} </p>
                </td>
                </tr>
                <tr>
                <td style="padding:10px; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0; background-color: #ffffff; width:40%;">
                    <p style="font-size: 12px; line-height: 18px; color:#000000; font-family: \'Poppins\', sans-serif; font-weight: 400;font-style: normal;margin:0; padding:0;"> Total amount </p>
                </td>
                <td style="padding:10px; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0; background-color: #ffffff;">
                    <p style="font-size: 12px; line-height: 18px; color:#000000; font-family: \'Poppins\', sans-serif; font-weight: 600;font-style: normal;margin:0; padding:0;"> {amount} </p>
                </td>
                </tr>
                <tr>
                <td style="padding:10px; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0; background-color: #f3f4ff; width:40%;">
                    <p style="font-size: 12px; line-height: 18px; color:#000000; font-family: \'Poppins\', sans-serif; font-weight: 400;font-style: normal;margin:0; padding:0;"> Delivery date </p>
                </td>
                <td style="padding:10px; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0; background-color: #f3f4ff;">
                    <p style="font-size: 12px; line-height: 18px; color:#000000; font-family: \'Poppins\', sans-serif; font-weight: 600;font-style: normal;margin:0; padding:0;"> {delivery_date} </p>
                </td>
                </tr>
                <tr>
                <td style="height:16px" height="16"></td>
                <td style="height:16px" height="16"></td>
                </tr>
            </table>
        </td>
        </tr>';

        $confirmation_email_based_action_po = '<tr>
        <td
          style="
            padding-top: 0;
            padding-right: 0;
            padding-bottom: 0;
            padding-left: 0;
            margin-top: 0;
            margin-right: 0;
            margin-bottom: 0;
            margin-left: 0;
            text-align:left;
          "
        >
          <p
            style="
              font-size: 14px;
              line-height: 21px;
              color: #000000;
              font-family: \'Poppins\', sans-serif;
              font-weight: 400;
              font-style: normal;
              margin: 0;
              padding: 0;
            "
          >
          You have successfully {invoice_status} {submitters_name}\'s purchase order.
          </p>
        </td>
      </tr>
      <tr>
        <td
          style="
            padding-top: 15px;
            padding-right: 0;
            padding-bottom: 0;
            padding-left: 0;
            margin-top: 0;
            margin-right: 0;
            margin-bottom: 0;
            margin-left: 0;
            text-align:left;
          "
        >
          <p
            style="
              font-size: 14px;
              line-height: 21px;
              color: #000000;
              font-family: \'Poppins\', sans-serif;
              font-weight: 400;
              font-style: normal;
              margin: 0;
              padding: 0;
            "
          >
            No further action is required at this stage and this
            action has been reflected your organisations Kloo
            account.
          </p>
        </td>
        </tr>';

        $whenWorkflowNotAssignedByAutomation = '<tr>
        <td
            style="padding:10px; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0; background-color: #F3F4FF; width:40%;text-align: left;">
            <p
                style="font-size: 12px; line-height: 18px; color:#000000;  font-family: \'Poppins\', sans-serif; font-weight: 400;font-style: normal;margin:0; padding:0;text-align: left;">
                Invoice number
            </p>
        </td>
        <td
            style="padding:10px; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0; background-color: #F3F4FF;text-align: left;">
            <p
                style="font-size: 12px; line-height: 18px; color:#000000;  font-family: \'Poppins\', sans-serif; font-weight: 600;font-style: normal;margin:0; padding:0;text-align: left;">
                {invoice_number}
            </p>
        </td>
    </tr>
    <tr>
        <td
            style="padding:10px; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0; background-color: #FFFFFF; width:40%;text-align: left;">
            <p
                style="font-size: 12px; line-height: 18px; color:#000000;  font-family: \'Poppins\', sans-serif; font-weight: 400;font-style: normal;margin:0; padding:0;text-align: left;">
                Owner
            </p>
        </td>
        <td
            style="padding:10px; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0; background-color: #FFFFFF;text-align: left;">
            <p
                style="font-size: 12px; line-height: 18px; color:#000000;  font-family: \'Poppins\', sans-serif; font-weight: 600;font-style: normal;margin:0; padding:0;text-align: left;">
                {owner}
            </p>
        </td>
    </tr>
    <tr>
        <td
            style="padding:10px; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0; background-color: #F3F4FF; width:40%;text-align: left;">
            <p
                style="font-size: 12px; line-height: 18px; color:#000000;  font-family: \'Poppins\', sans-serif; font-weight: 400;font-style: normal;margin:0; padding:0;text-align: left;">
                Supplier
            </p>
        </td>
        <td
            style="padding:10px; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0; background-color: #F3F4FF;text-align: left;">
            <p
                style="font-size: 12px; line-height: 18px; color:#000000;  font-family: \'Poppins\', sans-serif; font-weight: 600;font-style: normal;margin:0; padding:0;text-align: left;">
                {payee}
            </p>
        </td>
    </tr>
    <tr>
        <td
            style="padding:10px; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0; background-color: #FFFFFF; width:40%;text-align: left;">
            <p
                style="font-size: 12px; line-height: 18px; color:#000000;  font-family: \'Poppins\', sans-serif; font-weight: 400;font-style: normal;margin:0; padding:0;text-align: left;">
                Description
            </p>
        </td>
        <td
            style="padding:10px; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0; background-color: #FFFFFF;text-align: left;">
            <p
                style="font-size: 12px; line-height: 18px; color:#000000;  font-family: \'Poppins\', sans-serif; font-weight: 600;font-style: normal;margin:0; padding:0;text-align: left;">
                {description}
            </p>
        </td>
    </tr>
    <tr>
        <td
            style="padding:10px; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0; background-color: #F3F4FF; width:40%;text-align: left;">
            <p
                style="font-size: 12px; line-height: 18px; color:#000000;  font-family: \'Poppins\', sans-serif; font-weight: 400;font-style: normal;margin:0; padding:0;text-align: left;">
                Amount
            </p>
        </td>
        <td
            style="padding:10px; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0; background-color: #F3F4FF;text-align: left;">
            <p
                style="font-size: 12px; line-height: 18px; color:#000000;  font-family: \'Poppins\', sans-serif; font-weight: 600;font-style: normal;margin:0; padding:0;text-align: left;">
                {amount}
            </p>
        </td>
    </tr>
    <tr>
        <td style="height:16px" height="16"></td>
        <td style="height:16px" height="16"></td>
    </tr>
    </table>
    </td>
    </tr>
    <tr>
        <td style="width:100%;">
            <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation"
                style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; width:94%; text-align: left;">
                <tr>
                    <td
                        style="padding-top: 0; padding-right:0; padding-bottom:0; padding-left:0; margin-top: 0; margin-right:0; margin-bottom:0; margin-left:0;">
                        <p
                            style="font-size: 14px; line-height: 21px; color:#000000;  font-family: \'Poppins\', sans-serif; font-weight: 400;font-style: normal;margin:0; padding:0;">
                            Please {click here}
                            to
                            log
                            into the
                            platform and assign this invoice
                            to a workflow so it can be reviewed by the relevant users within
                            your organisation.
                        </p>
                    </td>
                </tr>
                <tr>
                    <td style="height:16px" height="16"></td>
                </tr>
            </table>
        </td>
    </tr>';

        $contents = [
            'card_req_acknowledgement_approval' => [
                'is_enabled' => 'enabled',
                'subject' => 'Kloo card request received',
                'event_description' => 'A user has been issued a card',
                'is_audit' => 'true',
                'is_approval_process' => 'false',
                'content' => '<span style="word-wrap: break-word;white-space: pre-wrap;">Your request for a Kloo card has been received is currently being reviewed.<br><br>No further action is required at this stage and you will be notified when your Kloo card has been approved.</span>',
            ],
            'card_req_awaiting_first_level_approval' => [
                'is_enabled' => 'enabled',
                'subject' => 'Kloo card request approval required',
                'event_description' => 'Card request awaiting team approval',
                'is_audit' => 'true',
                'is_approval_process' => 'false',
                'content' => "<span style='word-wrap: break-word;white-space: pre-wrap;'>{requester_name} from '{organization_name}' has requested a new Kloo card. <br><br>Please {click here} to view and approve any outstanding requests.</span>",
            ],
            'card_req_awaiting_final_level_approval' => [
                'is_enabled' => 'enabled',
                'subject' => 'Kloo card request approval required',
                'event_description' => 'Card request awaiting team approval',
                'is_audit' => 'true',
                'is_approval_process' => 'false',
                'content' => "<span style='word-wrap: break-word;white-space: pre-wrap;'>{requester_name} from '{organization_name}' has requested a new Kloo card.<br><br> Please {click here} to view and approve any outstanding requests.</span>",
            ],
            'card_issued' => [
                'is_enabled' => 'enabled',
                'subject' => 'You have been issued a Kloo card',
                'event_description' => 'A user has been issued a card',
                'is_audit' => 'true',
                'is_approval_process' => 'false',
                'content' => '<span style="word-wrap: break-word;white-space: pre-wrap;">{issuer_name} has issued you a new Kloo card! {click_this_link} to get started and to manage all your active Kloo cards.</span>',
            ],
            'account_balance_warning' => [
                'is_enabled' => 'enabled',
                'subject' => 'Account balance warning',
                'event_description' => 'Account balance warning',
                'is_audit' => 'true',
                'is_approval_process' => 'false',
                // 'content' => '<span style="word-wrap: break-word;white-space: pre-wrap;"><br/>{issuer_name} has issued you a new Kloo card! {click_this_link} to get started and to manage all your active Kloo cards.</span>',
                'content' => '<span style="word-wrap: break-word;white-space: pre-wrap;">The remaining balance on your Kloo debit account is  £{account_blance}. <br><br/>If your balance is less than the amount required to make a given purchase, this will result in a failed transaction. <br/><br/> Account Name: {account_name}<br/>Account No: {account_number}<br/>Sort Code: {sort_code}<br/>{Click here} to view your organisations recent expenses.</span>',
            ],
            'card_transaction_declined' => [
                'is_enabled' => 'enabled',
                'subject' => 'Your Kloo card transaction has been declined',
                'event_description' => 'Kloo card has beed declined',
                'is_audit' => 'true',
                'is_approval_process' => 'false',
                'content' => $transactionDeclinedContent,
            ],
            'card_transaction_declined_support' => [
                'is_enabled' => 'enabled',
                'subject' => 'Your Kloo card transaction has been declined',
                'event_description' => 'Kloo card has beed declined',
                'is_audit' => 'true',
                'is_approval_process' => 'false',
                'content' => $modulrTransactionDeclinedContent,
            ],
            'consent_expiration_warning' => [
                'is_enabled' => 'enabled',
                'subject' => 'Consent expiration warning',
                'event_description' => 'Consent expiration warning',
                'is_audit' => 'true',
                'is_approval_process' => 'false',
                'content' => '<span style="word-wrap: break-word;white-space: pre-wrap;">We hope this email finds you well. <br><br>Your consent for {institution_name} is set to expire soon. <br><br>To enable continued visualisations based on this data, please renew this consent before it expires on {expires_at}.<br><br/>To do this, simply log into your account and follow the steps to re-authorise your Open Banking access.</span>',
            ],
            'invoice_submitted' => [
                'is_enabled' => 'enabled',
                'subject' => 'Your invoice has been received',
                'event_description' => 'Kloo invoice has beed received',
                'is_audit' => 'true',
                'is_approval_process' => 'false',
                'content' => '<span style="word-wrap: break-word;white-space: pre-wrap;">Your invoice {{ $invoiceNumber }}@if(!empty($payeeName)) for {{ $payeeName }} @endif has been submitted and is currently being reviewed.<br><br>No further action is required at this stage and you will be notified of any updates.</span>',
            ],
            'invoice_has_completed_a_workflow' => [
                'is_enabled' => 'enabled',
                'subject' => 'Your invoice has been approved',
                'event_description' => 'Your invoice has been approved',
                'is_audit' => 'true',
                'is_approval_process' => 'false',
                'content' => '<span style="word-wrap: break-word;white-space: pre-wrap;">Your invoice {{ $invoiceNumber }}@if(!empty($payeeName)) for {{ $payeeName }} @endif has been approved! This will now be scheduled for payment by your organisation.<br><br>To view your invoices, please {click here}.</span>',
            ],
            'invoice_has_been_rejected' => [
                'is_enabled' => 'enabled',
                'subject' => 'Your invoice has been rejected',
                'event_description' => 'Your invoice has been rejected',
                'is_audit' => 'true',
                'is_approval_process' => 'false',
                'content' => '<span style="word-wrap: break-word;white-space: pre-wrap;">Your submitted invoice {{ $invoiceNumber }}@if(!empty($payeeName)) for {{ $payeeName }} @endif has been rejected @if(!empty($rejectReason)) for the following reason: <br><br> {{ $rejectReason }} @endif. <br><br> To resubmit this invoice, please {click here}.</span>',
            ],
            'invoice_has_approved_and_scheduled_for_payment' => [
                'is_enabled' => 'enabled',
                'subject' => 'Your invoice has been scheduled for payment',
                'event_description' => 'Your invoice has been scheduled for payment',
                'is_audit' => 'true',
                'is_approval_process' => 'false',
                'content' => '
                <span style="word-wrap: break-word;white-space: pre-wrap;">Your submitted invoice {{ $invoiceNumber }}@if(!empty($payeeName)) for {{ $payeeName }} @endif has been scheduled for payment and will be paid on {{ $expiresAt }}</span>',
            ],
            'invoice_approval' => [
                'is_enabled' => 'enabled',
                'subject' => 'Invoice approval required',
                'event_description' => 'Invoice approval required',
                'is_audit' => 'true',
                'is_approval_process' => 'false',
                'content' => $invoice_approval_content,
            ],
            'confirmation_email_based_action' => [
                'is_enabled' => 'enabled',
                'subject' => 'Invoice {invoice_status} confirmation',
                'event_description' => 'Invoice confirmation',
                'is_audit' => 'true',
                'is_approval_process' => 'false',
                'content' => $confirmation_email_based_action_content,
            ],
            'po_approval' => [
                'is_enabled' => 'enabled',
                'subject' => 'Purchase order approval required',
                'event_description' => 'Purchase order approval required',
                'is_audit' => 'true',
                'is_approval_process' => 'false',
                'content' => $po_approval_content,
            ],
            'invoice_modified' => [
                'is_enabled' => 'enabled',
                'subject' => 'Your invoice has been modified',
                'event_description' => 'Your invoice has been modified',
                'is_audit' => 'true',
                'is_approval_process' => 'false',
                'content' => '<span style="word-wrap: break-word;white-space: pre-wrap;">Your amount for invoice {{ $invoiceNumber }} has been changed from {{ $currencySymbol }}{{ $oldValue }} to {{ $currencySymbol }}{{ $newValue }} by {{ $requesterName }}.<br><br>@if(!empty($payeeName)) This invoice is being paid to {{ $payeeName }} @endif <br><br>To view this invoice, please {click here}.</span>',
            ],
            'invoice_payment_rescheduled' => [
                'is_enabled' => 'enabled', 
                'subject' => 'Your invoice payment has been rescheduled',
                'event_description' => 'Your invoice payment has been rescheduled',
                'is_audit' => 'true',
                'is_approval_process' => 'false',
                'content' => '<span style="word-wrap: break-word;white-space: pre-wrap;">Your submitted invoice {{ $invoiceNumber }}@if(!empty($payeeName)) for {{ $payeeName }} @endif has been rescheduled for payment and will be paid on {{ $expiresAt }}</span>',
            ],

            'confirmation_email_based_action_po' => [
                'is_enabled' => 'enabled',
                'subject' => 'Purchase order {invoice_status} confirmation',
                'event_description' => 'Purchase confirmation',
                'is_audit' => 'true',
                'is_approval_process' => 'false',
                'content' => $confirmation_email_based_action_po,
            ],
            'invoice_payment_receipt' => [
                'is_enabled' => 'enabled',
                'subject' => 'Payment made by {{ $organization }} for invoice number {{ $invoiceNumber }}',
                'event_description' => 'Invoice paid successfully',
                'is_audit' => 'true',
                'is_approval_process' => 'false',
                'content' => '<span style="word-wrap: break-word;white-space: pre-wrap;">
                    To whom it may concern,</br></br>
                    Please find attached the PDF remittence for the below payment. If you have any questions, please reach out to your regular finance / account contact.</br></br>
                    Invoice number - {{ $invoiceNumber }}</br>
                    Amount - {{ $currency . $amount }}</br>
                    Payment date - {{ date("d/m/Y", strtotime($paymentDate)) }}</br>
                </span>',
            ],
            'when_workflow_not_assigned_by_automation' => [
                'is_enabled' => 'enabled',
                'subject' => 'Invoice requiring assignment',
                'event_description' => 'A worklfow is not assigned automatically when invoice is created',
                'is_audit' => 'false',
                'is_approval_process' => 'false',
                'content' => $whenWorkflowNotAssignedByAutomation,
            ]

        ];
        $email_types = EmailType::all();

        $organizations = DB::table('organizations')
            ->select('id', 'organization_name', 'type')
            ->whereIn('type', ['modulr','others'])
            ->get();

        foreach ($organizations as $org) {
            foreach ($email_types as $email_type) {
               // if ($org->type == $email_type->belongs_to) {

                    $emailOrgNotification = EmailOrgNotification::where(
                        [
                            'organization_id' => $org->id,
                            'email_slug_id' => $email_type->id
                        ]
                    )->first();

                    if (empty($emailOrgNotification)) {
                        EmailOrgNotification::create([
                            'id' => Str::uuid()->toString(),
                            'email_slug_id' => $email_type->id,
                            'organization_id' => $org->id,
                            'is_enabled' => isset($contents[$email_type->slug]) ? $contents[$email_type->slug]['is_enabled'] : 'disabled',
                            'subject' => isset($contents[$email_type->slug]) ? $contents[$email_type->slug]['subject'] : '',
                            'event_description' => isset($contents[$email_type->slug]) ? $contents[$email_type->slug]['event_description'] : '',
                            'is_audit' => isset($contents[$email_type->slug]) ? $contents[$email_type->slug]['is_audit'] : 'false',
                            'content' => isset($contents[$email_type->slug]) ? $contents[$email_type->slug]['content'] : '',
                            'is_approval_process' => isset($contents[$email_type->slug]) ? $contents[$email_type->slug]['is_approval_process'] : 'false',
                        ]);
                    } else {
                        $emailOrgNotification->update([
                            'email_slug_id' => $email_type->id,
                            'organization_id' => $org->id,
                            'is_enabled' => isset($contents[$email_type->slug]) ? $contents[$email_type->slug]['is_enabled'] : 'disabled',
                            'subject' => isset($contents[$email_type->slug]) ? $contents[$email_type->slug]['subject'] : '',
                            'is_audit' => isset($contents[$email_type->slug]) ? $contents[$email_type->slug]['is_audit'] : 'false',
                            'content' => isset($contents[$email_type->slug]) ? $contents[$email_type->slug]['content'] : '',
                        ]);
                    }
               // } 
            }
        }
    }
}
