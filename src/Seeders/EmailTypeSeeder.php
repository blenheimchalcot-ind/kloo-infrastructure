<?php

namespace Kloo\Infrastructure\Seeders;

use Illuminate\Database\Seeder;
use Kloo\Infrastructure\Models\EmailType;
use Illuminate\Support\Str;
use Kloo\Infrastructure\Helpers\EnvironmentHelper;

class EmailTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EmailType::disableAuditing();
        $bccemail = 'kloosupport@getkloo.com';
        if (EnvironmentHelper::isProduction()) {
            $bccemail = 'support@getkloo.com';
        }
        $states =
            [
                [
                    'slug' => 'card_req_acknowledgement_approval',
                    'event' => 'Card Request Received',
                    'status' => 'active',
                    'belongs_to' => 'modulr'
                ],
                // [
                //     'id' => Str::uuid()->toString(),
                //     'slug' => 'card_transaction',
                //     'event' => 'A user has done expense on a card',
                //     'status' => 'active',
                //     'belongs_to' => 'modulr'
                // ],
                // [
                //     'id' => Str::uuid()->toString(),
                //     'slug' => 'card_fully_approved',
                //     'event' => 'A user has been issued a card',
                //     'status' => 'active',
                //     'belongs_to' => 'modulr'
                // ],
                [
                    'slug' => 'card_req_awaiting_first_level_approval',
                    'event' => 'Card request awaiting team approval',
                    'status' => 'active',
                    'belongs_to' => 'modulr'
                ],
                [
                    'slug' => 'card_req_awaiting_final_level_approval',
                    'event' => 'Card request awaiting Final approval',
                    'status' => 'active',
                    'belongs_to' => 'modulr'
                ],
                [
                    'slug' => 'card_issued',
                    'event' => 'A user has been issued a card',
                    'status' => 'active',
                    'belongs_to' => 'modulr'
                ],
                [
                    'slug' => 'card_transaction_declined',
                    'event' => 'Card transaction has been declined',
                    'status' => 'active',
                    'belongs_to' => 'modulr',
                    'bcc_emails' => $bccemail
                ],
                [
                    'slug' => 'account_balance_warning',
                    'event' => 'Account balance warning',
                    'status' => 'active',
                    'belongs_to' => 'modulr',
                    // 'bcc_emails' => $bccemail
                ],
                [
                    'slug' => 'card_transaction_declined_support',
                    'event' => 'Card transaction has been declined',
                    'status' => 'active',
                    'belongs_to' => 'modulr',
                    'bcc_emails' => $bccemail
                ],
                [
                    'slug' => 'consent_expiration_warning',
                    'event' => 'Consent expiration warning',
                    'status' => 'active',
                    'belongs_to' => 'modulr',
                    'bcc_emails' => $bccemail
                ], 
                [
                    'slug' => 'invoice_submitted',
                    'event' => 'Your invoice has been received',
                    'status' => 'active',
                    'belongs_to' => 'modulr',
                    'bcc_emails' => $bccemail
                ],
                [
                    'slug' => 'invoice_has_completed_a_workflow',
                    'event' => 'Your invoice has been approved',
                    'status' => 'active',
                    'belongs_to' => 'modulr',
                    // 'bcc_emails' => $bccemail
                ],
                [
                    'slug' => 'invoice_has_been_rejected',
                    'event' => 'Your invoice has been rejected',
                    'status' => 'active',
                    'belongs_to' => 'modulr',
                    'bcc_emails' => $bccemail
                ],
                [
                    'slug' => 'invoice_has_approved_and_scheduled_for_payment',
                    'event' => 'Your invoice has been scheduled for payment',
                    'status' => 'active',
                    'belongs_to' => 'modulr',
                    'bcc_emails' => $bccemail
                ],
                [
                    'slug' => 'invoice_approval',
                    'event' => 'Invoice approval required',
                    'status' => 'active',
                    'belongs_to' => 'modulr',
                    // 'bcc_emails' => $bccemail
                ],
                [
                    'slug' => 'invoice_modified',
                    'event' => 'Your invoice has been modified',
                    'status' => 'active',
                    'belongs_to' => 'modulr',
                    'bcc_emails' => $bccemail
                ],
                [
                    'slug' => 'confirmation_email_based_action',
                    'event' => 'Confirmation email',
                    'status' => 'active',
                    'belongs_to' => 'modulr',
                    'bcc_emails' => $bccemail
                ],
                [
                    'slug' => 'invoice_payment_rescheduled',
                    'event' => 'Your invoice payment has been rescheduled',
                    'status' => 'active',
                    'belongs_to' => 'modulr',
                    'bcc_emails' => $bccemail
                ],
                [
                    'slug' => 'po_approval',
                    'event' => 'Purchase order approval required',
                    'status' => 'active',
                    'belongs_to' => 'modulr',
                    'bcc_emails' => $bccemail
                ],
                [
                    'slug' => 'confirmation_email_based_action_po',
                    'event' => 'Confirmation email',
                    'status' => 'active',
                    'belongs_to' => 'modulr',
                    'bcc_emails' => $bccemail
                ],
                [
                    'slug' => 'invoice_payment_receipt',
                    'event' => 'Invoice paid successfully',
                    'status' => 'active',
                    'belongs_to' => 'modulr',
                    'bcc_emails' => $bccemail
                ],
                [
                    'slug' => 'when_workflow_not_assigned_by_automation',
                    'event' => 'When workflow is not assigned by automation',
                    'status' => 'active',
                    'belongs_to' => 'modulr',
                    // 'bcc_emails' => $bccemail
                ],
            ];

        foreach ($states as $state) {
            $slug_data = EmailType::where('slug', $state['slug'])->first();
            if (empty($slug_data)) {
                $state['id'] = Str::uuid()->toString();
                EmailType::create($state);
            }else{
                $slug_data->update($state);
            }
        }
    }
}
