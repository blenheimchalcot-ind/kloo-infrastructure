<?php

namespace Kloo\Infrastructure\MicroAppServices;

use Kloo\Infrastructure\Constants\CustomExpenseFieldStatus;
use Kloo\Infrastructure\DTOS\CreateUpdateCustomExpensesDTO;
use Kloo\Infrastructure\MicroAppServices\BaseMicroAppServices;
use Kloo\Infrastructure\Traits\MicroServiceAppResponseMerger;

class CustomExpenseFieldsMicroAppService extends BaseMicroAppServices
{
  use MicroServiceAppResponseMerger;

  protected function setMicroAppServiceName(): string
  {
    return "organization";
  }

  public function getResponseByMultipleIds(string $commaSeperatedIds, ?string $mergeKeyName = null): ?array
  {
    if ($mergeKeyName === "custom_form_field_details") {
      return $this->setURLPaths(["fetch-custom-validation"])->post(['id' => $commaSeperatedIds])->getJsonResponse();
    } else if ($mergeKeyName === "custom_expense_user_selection") {
      $payload = ['ids' => explode(",", $commaSeperatedIds), 'page_name' => "purchase_order"];
      return $this->setURLPaths(["custom-expense-user-selection"])->post($payload)->getJsonResponse();
    }
  }

  public function updateCustomExpenses(CreateUpdateCustomExpensesDTO $createUpdateCustomExpensesDTO): ?MicroAppServicesResponse
  {
    $payload = $createUpdateCustomExpensesDTO->toArray();
    $result = $this->setURLPaths(["custom-expense-transaction"])->put($payload);
    return $result;
  }

  public function createCustomExpenses(CreateUpdateCustomExpensesDTO $createUpdateCustomExpensesDTO): ?MicroAppServicesResponse
  {
    $result = $this->setURLPaths(["custom-expense-transaction"])->post($createUpdateCustomExpensesDTO->toArray());
    return $result;
  }

  public function updateCustomExpensesStatus(array|string $transactionId, $status = CustomExpenseFieldStatus::CUSTOM_EXPENSE_FIELD_COMMITTED): ?MicroAppServicesResponse
  {
    $transactionId = is_array($transactionId) ? $transactionId : explode(",", $transactionId);
    $result = $this->setURLPaths(["custom-expense-status", $status])->patch(["expense_ids" => $transactionId]);
    return $result;
  }
}
