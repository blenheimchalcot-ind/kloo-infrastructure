<?php

namespace Kloo\Infrastructure\Response;

use Kloo\Infrastructure\Constants\HttpStatusCodeConstant;
use Kloo\Infrastructure\Exceptions\HttpResponseException;
use Kloo\Infrastructure\Helpers\ArrayHelper;

class HttpResponseError extends HttpResponse
{
    public bool $isError = true;
    public ?array $errorData;

    public function __construct(HttpResponseException $exception)
    {
        parent::__construct(null, $exception->getMessage(), $exception->getCode());
        if (ArrayHelper::isArrayValid($exception->errorData)) {
            $this->errorData = $exception->errorData;
        }
    }
}
