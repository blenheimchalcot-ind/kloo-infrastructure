<?php

namespace Kloo\Infrastructure\Jobs\Middlewares;

use Kloo\Infrastructure\Helpers\UtilHelper;

class UserOrgIdSetterMiddleware
{

  public function handle($job, $next)
  {
    if (isset($job->sessionData)) {
      UtilHelper::setUserOrgIdAsQueryStrInCurrentRequest($job->sessionData['user_org_id']);
    }

    $next($job);
  }
}
