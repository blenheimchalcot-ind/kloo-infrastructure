<?php

namespace Kloo\Infrastructure\Jobs\Traits;

use Illuminate\Database\Eloquent\Model;
use Kloo\Infrastructure\Facades\RequestSessionFacade;
use Kloo\Infrastructure\Helpers\ArrayHelper;
use Kloo\Infrastructure\Log\Logger;

/** 
 * UniqueJobTrait.php
 */
trait SessionDataTrait
{
  public $sessionData = [];

  protected function setSessionData(): void
  {
    $this->sessionData = [
      "org_id" => RequestSessionFacade::getOrgIdFromQueryStrElseFromToken(),
      "user_org_id" => RequestSessionFacade::getUserOrgIdFromQueryStrElseFromToken(),
    ];
  }
}
